#Made by Jakub Olszewski 2022

import argparse
import VID2GIF

parser = argparse.ArgumentParser()

#input, name, fps = 24, width = None, height = None, start = 0, end = None

#Required args
parser.add_argument("-i", "--input_path", required=True, help="File name with path")
parser.add_argument("-n", "--output_name", required=True, help="Output file name [.gif will be added]")

#Optional args
parser.add_argument("-f", "--fps",    required=False, default = 24, type = int, nargs='?', 
                    help="FPS, if not given default is 24")
parser.add_argument("-w", "--width",  required=False, const = None, type = int, nargs='?', 
                    help="Width for out file (video will be resized), if none given, size will be original")
parser.add_argument("-H", "--height", required=False, const = None, type = int, nargs='?', 
                    help="Height for out file (video will be resized), if none given, size will be original")
parser.add_argument("-s", "--start",  required=False, default = 0, type = int, nargs='?', 
                    help="From which frame should the vid be exported (Default is 0)")
parser.add_argument("-e", "--end",    required=False, const = None, type = int, nargs='?', 
                    help="To which frame should the vid be exported (Default is None, full video is exported)")
parser.add_argument("-v", "--verbose",required=False, const = True, nargs='?', 
                    help="Type -v to see what is currently happening")

#Parse
args = parser.parse_args()

gif = VID2GIF.VID2GIF()

gif.convert(input   = args.input_path,
            name    = args.output_name,
            fps     = args.fps,
            width   = args.width,
            height  = args.height,
            start   = args.start,
            end     = args.end,
            verbose = args.verbose != None)

