#Made by Jakub Olszewski 2022

import cv2 as cv
import numpy as np
import imageio
import os

class VID2GIF:
    def __init__(self) -> None:
        pass

    def convert(self, input, name, fps = 24, width = None, height = None, start = 0, end = None, verbose = False) -> None:

        if verbose:
            print("Beginning process")

        #Get paths
        baseDir = os.getcwd()
        os.chdir(os.path.split(input)[0])

        if verbose:
            print("Clearing cache")

        #Clear cache
        os.chdir(os.path.join(baseDir,"TMP"))
        list = os.listdir()
        list = sorted(list, key=lambda x: int((x.split('\\')[-1]).split('.')[0]))
        for file in list:
            os.remove(file)

        if verbose:
            print("Initialise video input")

        #Initiate video
        try:
            cap = cv.VideoCapture(input)
            ret, frame = cap.read()
            if not ret:
                exit("An error occured while trying to open given file")    
        except:
            exit("An error occured while trying to open given file")

        #Resize
        if width == None:
            width = len(frame[0])
        if height == None:
            height = len(frame)

        if verbose:
            print("Exporting video frames")

        #Export images
        os.chdir(os.path.join(baseDir,"TMP"))
        out = []
        count = 0
        while(ret):
            if(count >= start):
                if (end != None):
                    if (count == end):
                        break
                res = cv.resize(frame,(width,height))
                
                frame = VID2GIF.myCode(frame)

                cv.imwrite(str(count)+".png", res)

            ret, frame = cap.read()
            count += 1


        list = os.listdir()
        list = sorted(list, key=lambda x: int((x.split('\\')[-1]).split('.')[0]))
        out2 = []

        if verbose:
            print("Collecting frames into list")

        #Transfer files to GIF
        for w in list:
            cv.imshow("Show",cv.imread(w))
            cv.waitKey(int(1000/fps))
            out2.append(imageio.imread(w))
        cv.destroyAllWindows()

        if verbose:
            print("Turning list into GIF (might take a while)")

        finalPath = os.path.join(baseDir,"Output",name+".gif")
        imageio.mimsave(finalPath, out2, format = 'GIF', fps = fps)

        if verbose:
            print("Clearing cache")

        #Clear cache
        os.chdir(os.path.join(baseDir,"TMP"))
        for file in list:
            os.remove(file)

        if verbose:
            print("Done!")

    def myCode(self, frame):

        '''HERE IS SPACE FOR YOUR CODE, HAVE FUN'''

        return frame