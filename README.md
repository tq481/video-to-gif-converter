# Video to GIF converter

Simple script to convert given video file to GIF file

## Basics

Script is based on `OpenCV` and `imageio`.
Python version 3.9.
Its pourpose is to convert given video file into GIF file.

## Before You start

Git clone existing repo and enter it
```bash
git clone https://gitlab.com/tq481/video-to-gif-converter.git
cd video-to-gif-converter
```
Next create necesary folders
```bash
mkdir Output && mkdir TMP
```
And everything is setup and ready to convert!

## How do I convert files?

Call in console
```
python3 main.py [flags]
```
or
```
py -3.9 main.py [flags]
```

Where flags are 
```
  -h, --help            show this help message and exit
  -i INPUT_PATH, --input_path INPUT_PATH
                        File name with path
  -n OUTPUT_NAME, --output_name OUTPUT_NAME
                        Output file name [.gif will be added]
  -f [FPS], --fps [FPS]
                        FPS, if not given default is 24
  -w [WIDTH], --width [WIDTH]
                        Width for out file (video will be resized), if none given, size will be original
  -H [HEIGHT], --height [HEIGHT]
                        Height for out file (video will be resized), if none given, size will be original
  -s [START], --start [START]
                        From which frame should the vid be exported (Default is 0)
  -e [END], --end [END]
                        To which frame should the vid be exported (Default is None, full video is exported)
  -v [VERBOSE], --verbose [VERBOSE]
                        Type -v to see what is currently happening
```

Main args are
 - `-i`, where path to the source file should be given.
 - `-o`, where output file name is given. `.gif` at the end will be added automatically.

## But wait, there's more!
If You know OCV and want to apply some effects to rework the video, You can do it!
In `VID2GIF.py` file there is method called `myCode`, where You can put everything You want to.

